package com.example.hxds.dr.service;

import com.example.hxds.common.util.PageUtils;

import java.util.HashMap;
import java.util.Map;

public interface DriverService {

    //注册新司机
    public String registerNewDriver(Map param);

    //更新保存司机实名认证信息
    public int updateDriverAuth(Map param);

    //司机脸部识别
    public String createDriverFaceModel(long driverId, String photo);

    //用户登录
    HashMap login(String code);

    //查询司机基础信息
    public HashMap searchDriverBaseInfo(long driverId);

    //查询司机数据分页
    public PageUtils searchDriverByPage(Map param);

    //查询司机实名认证信息
    public HashMap searchDriverAuth(long driverId);

    //查询司机实名认证信息摘要
    public HashMap searchDriverRealSummary(long driverId);

    //审批认证
    public int updateDriverRealAuth(Map param);

    public HashMap searchDriverBriefInfo(long driverId);

    public String searchDriverOpenId(long driverId);

}
