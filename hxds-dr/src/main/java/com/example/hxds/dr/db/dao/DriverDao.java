package com.example.hxds.dr.db.dao;


import com.example.hxds.dr.db.pojo.DriverSettingsEntity;
import com.example.hxds.dr.db.pojo.WalletEntity;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface DriverDao {

    //查询要注册的司机是否已经存在记录
    public long hasDriver(Map param);

    //插入注册数据
    public int registerNewDriver(Map param);

    //查询司机的主键值
    public String searchDriverId(String openId);

    //更新保存司机实名认证信息
    public int updateDriverAuth(Map param);

    //查询司机姓名和性别
    public HashMap searchDriverNameAndSex(long driverId);

    //更新司机表的archive字段值
    public int updateDriverArchive(long driverId);

    //司机登录
    public HashMap login(String openId);

    //查询司机基础信息
    public HashMap searchDriverBaseInfo(long driverId);

    //查询分页数据
    public ArrayList<HashMap> searchDriverByPage(Map param);

    //查询司机总数
    public long searchDriverCount(Map param);

    //查询司机实名认证信息
    public HashMap searchDriverAuth(long driverId);

    //查询司机实名认证信息摘要
    public HashMap searchDriverRealSummary(long driverId);

    //审批认证
    public int updateDriverRealAuth(Map param);

    public HashMap searchDriverBriefInfo(long driverId);

    public String searchDriverOpenId(long driverId);

}




