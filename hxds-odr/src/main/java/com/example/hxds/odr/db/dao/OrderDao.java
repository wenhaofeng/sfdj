package com.example.hxds.odr.db.dao;


import com.example.hxds.odr.db.pojo.OrderEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface OrderDao {

    //查询司机当天营业数据
    public HashMap searchDriverTodayBusinessData(long driverId);

    //插入订单
    public int insert(OrderEntity entity);

    //通过uuid查询订单
    public String searchOrderIdByUUID(String uuid);

    //司机抢单
    public int acceptNewOrder(Map param);

    //查询执行已抢单的订单信息
    public HashMap searchDriverExecuteOrder(Map param);

    //查询订单状态
    public Integer searchOrderStatus(Map param);

    //取消未接收的订单
    public int deleteUnAcceptOrder(Map param);

    //查询司机目前已接收的订单
    public HashMap searchDriverCurrentOrder(long driverId);

    //客户查询未完成已接单的订单
    public Long hasCustomerUnFinishedOrder(long customerId);

    //客户查询未接单的订单
    public HashMap hasCustomerUnAcceptOrder(long customerId);

    //查询订单信息用于司乘同显功能
    public HashMap searchOrderForMoveById(Map param);

    public int updateOrderStatus(Map param);

    public long searchOrderCount(Map param);

    public ArrayList<HashMap> searchOrderByPage(Map param);

    public HashMap searchOrderContent(long orderId);

    public ArrayList<String> searchOrderStartLocationIn30Days();

    public int updateOrderMileageAndFee(Map param);

    public long validDriverOwnOrder(Map param);

    public HashMap searchSettlementNeedData(long orderId);

    public HashMap searchOrderById(Map param);

    public HashMap validCanPayOrder(Map param);

    public int updateOrderPrepayId(Map param);

    public HashMap searchOrderIdAndStatus(String uuid);

    public HashMap searchDriverIdAndIncentiveFee(String uuid);

    public int updateOrderPayIdAndStatus(Map param);

    public int finishOrder(String uuid);

    public HashMap searchUuidAndStatus(long orderId);

    public int updateOrderAboutPayment(Map param);

    //查询司机与乘客和订单是否有关联
    public long validDriverAndCustomerOwnOrder(Map param);

    public ArrayList<HashMap> searchDriverOrderByPage(Map param);

    public long searchDriverOrderCount(Map param);

    public ArrayList<HashMap> searchCustomerOrderByPage(Map param);

    public long searchCustomerOrderCount(Map param);
}




