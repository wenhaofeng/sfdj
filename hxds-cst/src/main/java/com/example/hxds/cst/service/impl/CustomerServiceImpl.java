package com.example.hxds.cst.service.impl;

import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.example.hxds.common.exception.HxdsException;
import com.example.hxds.common.util.MicroAppUtil;
import com.example.hxds.cst.db.dao.CustomerDao;
import com.example.hxds.cst.service.CustomerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author fengwenhao
 * @Date 2023/7/18 17:33
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Resource
    private CustomerDao customerDao;

    @Resource
    private MicroAppUtil microAppUtil;


    @Override
    @Transactional
    @LcnTransaction
    public String registerNewCustomer(Map param) {
        String code = param.get("code").toString();
        String openId = microAppUtil.getOpenId(code);
        HashMap tempParam=new HashMap();
        tempParam.put("openId",openId);
        if(customerDao.hasCustomer(tempParam)!=0){
            throw new HxdsException("该微信已注册");
        }
        param.put("openId",openId);
        customerDao.registerNewCustomer(param);
        String customerId = customerDao.searchCustomerId(openId);
        return customerId;
    }

    @Override
    public String login(String code) {
        String openId = microAppUtil.getOpenId(code);
        String customerId = customerDao.login(openId);
        customerId=customerId!=null?customerId:"";
        return customerId;
    }

    @Override
    public HashMap searchCustomerInfoInOrder(long customerId) {
        HashMap map = customerDao.searchCustomerInfoInOrder(customerId);
        return map;
    }

    @Override
    public HashMap searchCustomerBriefInfo(long customerId) {
        HashMap map = customerDao.searchCustomerBriefInfo(customerId);
        return map;
    }

    @Override
    public String searchCustomerOpenId(long customerId) {
        String openId = customerDao.searchCustomerOpenId(customerId);
        return openId;
    }
}
