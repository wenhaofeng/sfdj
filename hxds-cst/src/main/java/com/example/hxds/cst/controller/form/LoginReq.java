package com.example.hxds.cst.controller.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @Description
 * @Author fengwenhao
 * @Date 2023/8/16 11:23
 */
@Data
public class LoginReq {

    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^1\\d{10}$", message = "手机号内容不正确")
    private String phone;

    @NotBlank(message = "密码不能为空")
    private String password;
}
